//
// Created by benjamin on 09.08.21.
//

#include <iostream>
#include <fstream>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/array.hpp>

#include "common_types.h"
#include "common_functions.h"

int main(int argc, char *argv[]) {
	if (argc < 2) {
		std::cerr << "Please provide one argument, the simulation file name" << std::endl;
	}
	std::string inputFile(argv[1]);
	std::ifstream ifs(inputFile);
	boost::archive::text_iarchive ia(ifs);

	MSA msa;

	ia >> msa;

	print_MSA(msa);

}