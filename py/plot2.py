import numpy as np
import matplotlib.pyplot as plt


def get_cmap(n, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, n)


w = np.loadtxt("w_log")
real = np.loadtxt("real_theta_w.csv")
counts = np.loadtxt("counts_w.csv")

x = np.arange(0,w.shape[1])

p = w.shape[0]

#choose = np.random.choice(p, number, replace=False)

#choose = [c if counts[int(c/9)][c%9] > 100 for range(0,len(w)]

choose = list(filter(lambda c : counts[int(c/9)][c%9] > 800, range(0,len(w))))

print(choose)

colors = get_cmap(len(choose))

for i,c in enumerate(choose):
	plt.plot(x, w[c], c = colors(i))
	pair = int(c / 9)
	amino = c % 9
	plt.plot(x, np.full(x.shape,real[pair][amino]), c = colors(i))
plt.show()

