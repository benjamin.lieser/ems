import numpy as np
import matplotlib.pyplot as plt
import sys

rundir = "../" + sys.argv[1]

real_filename = rundir + "/real_theta"
estimated_filename = rundir + "/estimated_theta"

A = 3
L = 6

def cov(v, w):
	P = w.shape[0]
	cov = np.empty((P,A*A))
	p = 0
	for i in range(L):
		for j in range(i+1, L):
			ab = np.empty((A,A))
			for a in range(A):
				for b in range(A):
					ab[a,b] = np.exp(v[i][a] + v[j][b] + w[p][a*A+b])
			ab = ab / np.sum(ab)
			
			for a in range(A):
				for b in range(A):
					cov[p][A*a + b] = (ab[a,b] - np.sum(ab[a]) * np.sum(ab[:,b]))
			p += 1
	return cov

def norm_v(v):
	result = v.copy();
	for i in result:
		i -= i[0]
	return result

r_v = np.loadtxt(real_filename + "_v.csv")
r_w = np.loadtxt(real_filename + "_w.csv")

e_v = np.loadtxt(estimated_filename + "_v.csv")
e_w = np.loadtxt(estimated_filename + "_w.csv")

fig,ax = plt.subplots(2)

r_cov = cov(r_v, r_w)
e_cov = cov(e_v, e_w)

ax[0].imshow(r_cov)
ax[1].imshow(e_cov)

plt.show()
