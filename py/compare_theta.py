import numpy as np
import matplotlib.pyplot as plt
import sys

rundir = "../" + sys.argv[1]

real_filename = rundir + "/real_theta"
estimated_filename = rundir + "/estimated_theta"


def norm_v(v):
	result = v.copy();
	for i in result:
		i -= i[0]
	return result

r_v = np.loadtxt(real_filename + "_v.csv")
r_w = np.loadtxt(real_filename + "_w.csv")

print(r_w.shape)

e_v = np.loadtxt(estimated_filename + "_v.csv")
e_w = np.loadtxt(estimated_filename + "_w.csv")

fig,ax = plt.subplots(2)

#ax[0].imshow(norm_v(r_v))
#ax[1].imshow(norm_v(e_v))

#plt.show()

ax[0].imshow(r_w)
ax[1].imshow(e_w)

plt.show()
