import numpy as np
from Bio import Phylo

def AminoStringToNumberArray(string):
	AMINO_ACIDS = "ARNDCQEGHILKMFPSTWYV-"
	result = np.empty(len(string))
	for i in range(0,len(string)):
		result[i] = AMINO_ACIDS.find(string[i])
	return result

def read_alignment(filename):
	data = np.loadtxt(filename, dtype=str)
	N = len(data)
	if N == 0:
		raise Exceptions('attempted to load empty MSA')
	L = len(data[0])
	msa = np.empty((N,L))
	for i in range(0,N):
		msa[i] = AminoStringToNumberArray(data[i])
	return msa
	
def all_parents(tree):
    parents = {}
    for clade in tree.find_clades(order='level'):
        for child in clade:
            parents[child] = clade
    return parents
	
def read_tree(filename):
	tree = Phylo.read(filename, 'newick')
	
	treeSize = len(list(tree.find_clades()))
	npTree = np.empty((treeSize,2))
	N = tree.count_terminals() #Number of leaves, we use this variable to index the hidden nodes
	#Numerate all the hidden nodes:
	for node in tree.get_nonterminals():
		node.name = str(N)
		N += 1
	
	parents = all_parents(tree)
	
	
	for node in tree.find_clades():
		id = int(node.name)
		t = node.branch_length
		if node == tree.root:
			npTree[id] = [-1.0, t]
			continue;
		parentId = int(parents[node].name)
		npTree[id] = [parentId, t]
	return npTree
