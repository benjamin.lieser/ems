import numpy as np
import matplotlib.pyplot as plt
import sys

rundir = "../" + sys.argv[1]


w = np.loadtxt(rundir + "/w_log")
real = np.loadtxt(rundir + "/real_theta_w.csv")
estimated = np.loadtxt(rundir + "/estimated_theta_w.csv")
counts = np.loadtxt(rundir + "/counts_w.csv")

x = np.arange(0,w.shape[1])

real = real.flatten()
estimated = estimated.flatten()

max_val = max(np.max(real), np.max(estimated))

plt.scatter(real, estimated)
plt.plot([-max_val,max_val], [-max_val,max_val])
plt.show()
