from read_utils import *
import sys
import numpy as np

tree = read_tree(sys.argv[1])

f = open(sys.argv[2], "w")

f.write(str(len(tree)) + "\n")

for row in tree:
	f.write(str(int(row[0])) + " " + str(row[1])+"\n")
