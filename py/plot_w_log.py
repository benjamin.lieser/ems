import numpy as np
import sys
import matplotlib.pyplot as plt

rundir = "../" + sys.argv[1]

A = 3

w = np.loadtxt(rundir + "/w_log")

x = np.arange(0,w.shape[1])

for p in range(0,w.shape[0] // (A*A)):
	for a in range(A):
		for b in range(A):
			plt.plot(x, w[p * A * A + a*A + b])
plt.show()
