from read_utils import *
import pythonInput
import sys
import numpy as np

tree = read_tree(sys.argv[1])
msa = read_alignment(sys.argv[2])
lam = float(sys.argv[3])


pythonInput.serialize(msa, tree, lam, sys.argv[4])
