import numpy as np
import matplotlib.pyplot as plt
import sys

rundir = "../" + sys.argv[1]

A = 3
L = 6

def cov(v, w):
	result = []
	p = 0
	for i in range(L):
		for j in range(i+1, L):
			ab = np.empty((A,A))
			for a in range(A):
				for b in range(A):
					ab[a,b] = np.exp(v[i][a] + v[j][b] + w[p][a*A+b])
			ab = ab / np.sum(ab)
			
			for a in range(A):
				for b in range(A):
					result.append(ab[a,b] - np.sum(ab[a]) * np.sum(ab[:,b]))
			p += 1
	return np.array(result).flatten()
	
def theta(v, w):
	result = []
	p = 0
	for i in range(L):
		for j in range(i+1, L):
			ab = np.empty(A*A)
			for a in range(A):
				for b in range(A):
					ab[a*A + b] = np.exp(v[i][a] + v[j][b] + w[p][a*A+b])
			ab = ab / np.sum(ab)
			result.append(ab)
			p += 1
	return np.array(result).flatten()
			

w = np.loadtxt(rundir + "/w_log")
real = np.loadtxt(rundir + "/real_theta_w.csv")
estimated = np.loadtxt(rundir + "/estimated_theta_w.csv")
real_v = np.loadtxt(rundir + "/real_theta_v.csv")
estimated_v = np.loadtxt(rundir + "/estimated_theta_v.csv")
counts = np.loadtxt(rundir + "/counts_w.csv")

x = np.arange(0,w.shape[1])

real = theta(real_v, real)
estimated = theta(estimated_v, estimated)

print(np.corrcoef(real, estimated))

max_val = max(np.max(real), np.max(estimated))

plt.scatter(real, estimated)
plt.plot([-max_val,max_val], [-max_val,max_val])
plt.show()
