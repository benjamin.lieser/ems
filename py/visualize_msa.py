import numpy as np
import matplotlib.pyplot as plt
import sys

from sklearn.decomposition import PCA

rundir = "../" + sys.argv[1]

A = 3

msa = np.loadtxt(rundir + "/msa.aln")

L = msa.shape[1]

def sparse_encoding(msa):
	N = msa.shape[0]
	sparse = np.zeros((N,A*L))
	for (n,seq) in enumerate(msa):
		for (i,amino) in enumerate(seq):
			sparse[n][A*i + int(amino)] = 1
	return sparse

sparse = sparse_encoding(msa)

pca = PCA(n_components=2)
dim2 = pca.fit_transform(sparse)

plt.scatter(dim2[:,0], dim2[:,1])
plt.show()
