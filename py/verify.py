import numpy as np
from scipy.stats import chisquare

data = np.genfromtxt('../verify_g')

data = data[data[:,0]>0.5] #Filter small values

expect = data[:,0]
observ = data[:,1]

test = (expect - observ)**2 / expect

print(test[test > 1])
