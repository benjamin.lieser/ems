#!/bin/bash
rm $1 -r
mkdir $1/build -p
cd $1/build
cmake -DCMAKE_BUILD_TYPE=Release ../..
make p_sampling
make testing
cd ..
./build/p_sampling ../test_data/1024.tree 6
./build/testing simulation
