import os, sys
import configparser

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

config = configparser.ConfigParser()
config_file = sys.argv[1]
config.read(config_file)

rundir = sys.argv[2]

A = int(config['parameter_gen']['A'])
L = int(config['parameter_gen']['L'])

if os.path.isdir(rundir):
	print(rundir + " already exists")
	sys.exit()

#Write the correct value for A in A.h, this in needed because this value needs to be a compile time constant

f = open("A.h", "w")
f.write("const unsigned int A = " + str(A) + ";\n")
f.close()
	
os.system("mkdir " + rundir)
os.system('cp ' + config_file + ' ' + rundir)
with cd(rundir):
	os.system("mkdir build")
	with cd("build"):
		os.system("cmake -DCMAKE_BUILD_TYPE=Release ../..")
		os.system("make p_sampling")
		os.system("make testing")
	os.system('./build/p_sampling ' + config_file)
	os.system('./build/testing ' + config_file + ' simulation')

