//
// Created by benjamin on 01.03.21.
//

#ifndef EMS_FELSENSTEINNODE_H
#define EMS_FELSENSTEINNODE_H

#include <array>
#include "common_types.h"

template<typename F>
struct FelsensteinNode {
	std::array<F,A> precomp; //The a'th entry is p(X_n | x_n = a)
	std::array<F,A> log_precomp;
	F t = 0; //The time to the parent
	bool hidden = false; //Is this a hidden node
	amino data = 0; //The data of the node, if it is not a hidden node
	std::array<int,2> next = {-1,-1}; //The index of children of this node. -1 indicates no child.
};

#endif //EMS_FELSENSTEINNODE_H
