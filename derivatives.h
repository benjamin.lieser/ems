//
// Created by benjamin on 30.03.21.
//

#ifndef EMS_DERIVATIVES_H
#define EMS_DERIVATIVES_H

#include <torch/torch.h>


#include "common_types.h"
#include "common_functions.h"
#include "U.h"
#include "Theta.h"
#include "Phi.h"


//Adds the derivatives to d
template<typename F>
void d_theta_one_sample(const MSA &sample, const PhyloTree &tree, const U<F> &u, const Theta<F> &theta, Theta<F> &d_sum) {
	unsigned int L = sample[0].size();
	unsigned int N = sample.size();

	std::vector<std::vector<std::array<F, A>>> epsilon(N, std::vector<std::array<F, A>>(L));

	for (unsigned int m = 0; m < N; m++) {
		for (unsigned int k = 0; k < L; k++) {
			for (amino a = 0; a < A; a++) {
				epsilon[m][k][a] = theta.v[k][a];
				for (unsigned int j = 0; j < L; j++) {
					if (j == k) continue;
					if (j > k) {
						epsilon[m][k][a] += theta.w[theta.pair_to_id(k, j)][a * A + sample[m][j]];
					} else {
						epsilon[m][k][a] += theta.w[theta.pair_to_id(k, j)][sample[m][j] * A + a];
					}
				}
			}
		}
	}

	std::vector<std::vector<std::array<F, A>>> gamma1(N, std::vector<std::array<F, A>>(L));

	for (unsigned int m = 0; m < N; m++) {
		for (unsigned int i = 0; i < L; i++) {
			amino xmi = sample[m][i];
			for (amino a = 0; a < A; a++) {
				if (a == xmi) {
					gamma1[m][i][a] = 0;
				} else {
					gamma1[m][i][a] = u.u_i(xmi, a) * d_g(0.5 * (epsilon[m][i][a] - epsilon[m][i][xmi]));
				}
			}
		}
	}

	std::vector<std::vector<F>> gamma(N, std::vector<F>(L));

	for (unsigned int m = 0; m < N; m++) {
		for (unsigned int i = 0; i < L; i++) {
			gamma[m][i] = 0;
			for (amino c = 0; c < A; c++) {
				gamma[m][i] += gamma1[m][i][c];
			}
		}
	}

	Theta<F> d(L);

	//v
	for (unsigned int i = 0; i < L; i++) {
		F v_sum = 0;
		for (amino a = 0; a < A; a++) {
			v_sum += std::exp(theta.v[i][a]);
		}
		for (amino a = 0; a < A; a++) {
			for (unsigned int n = 0; n < N; n++) {
				auto[m, t] = tree[n];
				if (m == -1) {
					d.v[i][a] += delta<F>(sample[n][i], a);
					d.v[i][a] -= std::exp(theta.v[i][a]) / v_sum;
					continue;
				}
				amino xmi = sample[m][i];
				amino xni = sample[n][i];
				if (xni == xmi) {
					d.v[i][a] -= 0.5 * (gamma1[m][i][a] - delta<F>(xmi, a) * gamma[m][i]) * t;
				} else {
					F epsilons = 0.5 * (epsilon[m][i][xni] - epsilon[m][i][xmi]);
					d.v[i][a] += 0.5 * (delta<F>(xni, a) - delta<F>(xmi, a)) * d_g(epsilons) / g(epsilons);
				}
			}
		}
	}

	//w
	unsigned int p = 0;
	for (unsigned int i = 0; i < L; i++) {
		for (unsigned int j = i + 1; j < L; j++) {
			F w_sum = 0;
			for (amino a = 0; a < A; a++) {
				for (amino b = 0; b < A; b++) {
					w_sum += std::exp(theta.v[i][a] + theta.v[j][b] + theta.w[p][a * A + b]);
				}
			}
			for (unsigned int n = 0; n < N; n++) {
				auto[m, t] = tree[n];
				if (m == -1) {
					d.w[p][sample[n][i] * A + sample[n][j]] += 1.0;
					for(amino a = 0; a < A; a++) {
						for(amino b = 0; b < A; b++) {
							d.w[p][a * A + b] -= std::exp(theta.v[i][a] + theta.v[j][b] + theta.w[p][a * A + b]) / w_sum;
						}
					}
					continue;
				}
				amino xmi = sample[m][i];
				amino xni = sample[n][i];
				amino xmj = sample[m][j];
				amino xnj = sample[n][j];
				for (amino a = 0; a < A; a++) {
					if (xni == xmi) {
						d.w[p][a * A + xmj] -= 0.5 * (gamma1[m][i][a] - delta<F>(xmi, a) * gamma[m][i]) * t;
					} else {
						F epsilons = 0.5 * (epsilon[m][i][xni] - epsilon[m][i][xmi]);
						d.w[p][a * A + xmj] += 0.5 * (delta<F>(xni, a) - delta<F>(xmi, a)) * d_g(epsilons) / g(epsilons);
					}
				}
				for (amino b = 0; b < A; b++) {
					if (xnj == xmj) {
						d.w[p][xmi * A + b] -= 0.5 * (gamma1[m][j][b] - delta<F>(xmj, b) * gamma[m][j]) * t;
					} else {
						F epsilons = 0.5 * (epsilon[m][j][xnj] - epsilon[m][j][xmj]);
						d.w[p][xmi * A + b] += 0.5 * (delta<F>(xnj, b) - delta<F>(xmj, b)) * d_g(epsilons) / g(epsilons);
					}
				}
			}
			p++;
		}
	}
	#pragma omp critical(theta)
	{
		d_sum.add(d);
	}
}

template<typename F>
void d_theta(const std::vector<MSA> &samples, const PhyloTree &tree, const U<F> &u, at::Tensor v, at::Tensor w) {
	unsigned int L = samples[0][0].size();

	Theta<F> theta(L);
	theta.from_tensor(v, w);

	Theta<F> d(L);

	#pragma omp parallel for default(none) shared(tree, u, theta, samples, d)
	for (auto &sample : samples) {
		d_theta_one_sample(sample, tree, u, theta, d);
	}
	auto[dv, dw] = d.to_tensor();

	dv /= -(double) samples.size();
	dw /= -(double) samples.size();

	v.mutable_grad() = dv;
	w.mutable_grad() = dw;
}

template<typename F>
F epsilon(const std::vector<amino> &seq, const Theta<F> &theta) {
	F sum = 0;
	unsigned int L = seq.size();
	for (unsigned int i = 0; i < L; i++) {
		sum += theta.v[i][seq[i]];
	}
	for (unsigned int i = 0; i < L; i++) {
		for (unsigned int j = i + 1; j < L; j++) {
			sum += theta.wij(i, j, seq[i], seq[j]);
		}
	}
	return sum;
}

//Cacluates epsilon(x_{-i}c) - epsilon(x)
template<typename F>
F epsilon_diff(const std::vector<amino> &seq, const Theta<F> &theta, const unsigned int i, const amino c) {
	F sum = theta.v[i][c] - theta.v[i][seq[i]];
	for(unsigned int j = 0; j < seq.size(); j++) {
		if(j == i) continue;
		sum += theta.wij_checked(i,j,c,seq[j]) - theta.wij_checked(i, j, seq[i], seq[j]);
	}
	return sum;
}

//log(q(x_n|x_m, phi))
template<typename F>
F log_q_xn_xm(const MSA &sample, const U<F> &u, const Phi<F> &phi, unsigned int n, unsigned int m, F t) {
	F sum = 0;
	unsigned int L = sample[0].size();
	for (unsigned int i = 0; i < L; i++) {
		amino xi = sample[m][i];
		amino yi = sample[n][i];
		if (xi != yi) {
			sum += phi.f[i] + std::log(u.u_i(xi, yi)) + std::log(t) + log_g(0.5 * (phi.r[i][yi] - phi.r[i][xi]));
		} else {
			for (amino a = 0; a < A; a++) {
				if (a == xi) continue;
				sum -= std::exp(phi.f[i]) * u.u_i(xi, a) * g(0.5 * (phi.r[i][a] - phi.r[i][xi])) * t;
			}
		}
	}
	return sum;
}

//log(p(x_n|x_m, theta))
template<typename F>
F log_p_xn_xm(const MSA &sample, const U<F> &u, const Theta<F> &theta, unsigned int n, unsigned int m, F t) {
	F sum = 0;
	const auto &x = sample[m];
	const auto &y = sample[n];
	unsigned int L = sample[0].size();
	for (unsigned int i = 0; i < L; i++) {
		amino xi = x[i];
		amino yi = y[i];
		if (xi == yi) {
			for (amino a = 0; a < A; a++) {
				if (a == xi) continue;
				sum -= u.u_i(xi, a) * g(0.5 * epsilon_diff(x, theta, i, a)) * t;
			}
		} else {
			sum += u.log_u(xi, yi);
			sum += std::log(t);
			sum += log_g(0.5 * epsilon_diff(x, theta,i, yi));
		}
	}
	return sum;
}

template<typename F>
std::pair<std::vector<AA_array<F>>, std::vector<A_array<F>>> calculate_beta(const U<F> &u, const Phi<F> &phi, const unsigned int L) {
	std::vector<AA_array<F>> beta(L);

	std::vector<A_array<F>> beta2(L);

	for(unsigned int i = 0; i < L; i++) {
		for(amino b = 0; b < A; b++) {
			for(amino c = 0; c < A; c++) {
				beta[i][b][c] =  0.5 * std::exp(phi.f[i]) * u.u_i(c, b) * d_g(0.5 * (phi.r[i][b] - phi.r[i][c]));
			}
		}
	}

	for(unsigned int i = 0; i < L; i++) {
		for(amino c = 0; c < A; c++) {
			for(amino b = 0; b < A; b++) {
				beta2[i][c] += beta[i][b][c];
			}
		}
	}

	return {beta, beta2};
}

template<typename F>
void d_log_q(const MSA &sample, const PhyloTree &tree, const U<F> &u, const Phi<F> &phi, Phi<F> &d_sum, F multi, std::vector<AA_array<F>> &beta, std::vector<A_array<F>> &beta2) {
	unsigned int L = sample[0].size();

	Phi<F> d(L);

	for (unsigned int n = 0; n < tree.size(); n++) {
		auto[m, t] = tree[n];
		if (m == -1) {
			for (unsigned int i = 0; i < L; i++) {
				F sum = 0;
				for (amino b = 0; b < A; b++) {
					sum += std::exp(phi.r[i][b]);
				}
				for (amino a = 0; a < A; a++) {
					d.r[i][a] += delta<F>(a, sample[n][i]) * multi;
					d.r[i][a] -= std::exp(phi.r[i][a]) * multi / sum;
				}
			}
		} else {
			for (unsigned int i = 0; i < L; i++) {
				amino xni = sample[n][i];
				amino xmi = sample[m][i];

				//f
				if (xni == xmi) {
					for (amino b = 0; b < A; b++) {
						if (b == xni) continue;
						d.f[i] -= std::exp(phi.f[i]) * u.u_i(xni, b) * g(0.5 * (phi.r[i][b] - phi.r[i][xni])) * t * multi;
					}
				} else {
					d.f[i] += multi;
				}

				//r
				for (amino a = 0; a < A; a++) {
					if (xni == xmi) {
						d.r[i][a] -= t * (beta[i][a][xni] - delta<F>(a,xni)*beta2[i][xni]) * multi;
					} else {
						F inner = 0.5 * (phi.r[i][xni] - phi.r[i][xmi]);
						d.r[i][a] += d_g(inner) / g(inner) * 0.5 * (delta<F>(xni, a) - delta<F>(xmi, a)) * multi;
					}
				}
			}
		}
	}

	#pragma omp critical(Phi)
	{
		d_sum.add(d);
	}
}


template<typename F>
void
d_phi(const std::vector<MSA> &samples, const PhyloTree &tree, const U<F> &u, const at::Tensor &v, const at::Tensor &w,
	  at::Tensor &r, at::Tensor &f) {
	static_assert(std::is_same<F, double>::value, "For now this only works on double");

	unsigned int S = samples.size();
	unsigned int L = samples[0][0].size();

	std::vector<std::vector<F>> ln_trans(tree.size(), std::vector<F>(S, 0.0));
	std::vector<F> means(tree.size());

	Theta<F> theta(L);
	theta.from_tensor(v, w);

	Phi<F> phi(L);
	phi.from_tensor(r, f);

	for (unsigned int n = 0; n < tree.size(); n++) {
		auto[m, t] = tree[n];
		if (m == -1) continue;
		//#pragma omp parallel for default(none) shared(m, u, t, samples, n, ln_trans, theta, phi)
		for (unsigned int s = 0; s < samples.size(); s++) {
			auto p = log_p_xn_xm(samples[s], u, theta, n, m, t);
			auto q = log_q_xn_xm(samples[s], u, phi, n, m, t);

			ln_trans[n][s] = p - q;
		}
		means[n] = std::accumulate(ln_trans[n].begin(), ln_trans[n].end(), 0.0) / S;
	}


	Phi<F> d(L);

	auto beta = calculate_beta(u, phi, L);

	#pragma omp parallel for default(none) shared(tree, u, phi, samples, d, ln_trans, means, beta)
	for (unsigned int s = 0; s < samples.size(); s++) {
		F sum = 0;
		for (unsigned int n = 0; n < tree.size(); n++) {
			auto[m, t] = tree[n];
			if (m == -1) continue;
			sum += ln_trans[n][s] - means[n];
		}

		d_log_q(samples[s], tree, u, phi, d, sum, beta.first, beta.second);

	}

	auto[r_grad, f_grad] = d.to_tensor();

	r.mutable_grad() = r_grad / -(double) S;
	f.mutable_grad() = f_grad / -(double) S;
}

#endif //EMS_DERIVATIVES_H
