//
// Created by benjamin on 24.06.21.
//

#include <iostream>
#include "common_functions.h"

int main() {
	for(double x = -5; x <= 5; x += 0.00025) {
		std::cout << std::exp(log_g(x)) - g(x) << " ";
	}
	std::cout << std::endl;
}