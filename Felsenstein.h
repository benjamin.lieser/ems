//
// Created by benjamin on 01.03.21.
//

#ifndef EMS_FELSENSTEIN_H
#define EMS_FELSENSTEIN_H

#include <vector>

#include "common_types.h"
#include "common_functions.h"
#include "FelsensteinNode.h"

template<typename F>
class Felsenstein {
private:
	//TODO this does not correctly compute at the root, because it is also a leave. Fix this if this gets relevant
	template<typename Transition>
	void calculate_edge(int index, const Transition &trans) {
		FelsensteinNode<F> &edge = nodes[index];
		if(edge.next[0] != -1) calculate_edge(edge.next[0], trans);
		if(edge.next[1] != -1) calculate_edge(edge.next[1], trans);

		//Linspace
		if(edge.hidden) { //This is a hidden node
			for(amino a = 0; a < A; a++) {
				F factor1 = 1.0;
				F factor2 = 1.0;

				if(edge.next[0] != -1) {
					factor1 = 0;
					for(amino b = 0; b < A; b++) {
						factor1 += nodes[edge.next[0]].precomp[b] * trans(b, a, edge.t, col);
					}
				}

				if(edge.next[1] != -1) {
					factor2 = 0;
					for(amino b = 0; b < A; b++) {
						factor2 += nodes[edge.next[1]].precomp[b] * trans(b, a, edge.t, col);
					}
				}

				edge.precomp[a] = factor1 * factor2;
			}
		} else { //This is a leave node
			edge.precomp.fill(0.0);
			edge.precomp[edge.data] = 1.0;
		}


		//Logspace
		if(edge.hidden) { //This is a hidden node
			for(amino a = 0; a < A; a++) {
				edge.log_precomp[a] = 0;

				std::array<F,A> space;

				if(edge.next[0] != -1) {
					for(amino b = 0; b < A; b++) {
						space[b] = nodes[edge.next[0]].log_precomp[b] + trans.log(b, a, std::log(edge.t), col);
					}
					edge.log_precomp[a] += logSumExpNat(space.data(), A);
				}

				if(edge.next[1] != -1) {
					for(amino b = 0; b < A; b++) {
						space[b] = nodes[edge.next[1]].log_precomp[b] + trans.log(b, a, std::log(edge.t), col);
					}
					edge.log_precomp[a] += logSumExpNat(space.data(), A);
				}
			}
		} else { //This is a leave node
			edge.log_precomp.fill(log0);
			edge.log_precomp[edge.data] = 0;
		}
	}

	//Populates the nodes array and assigns root_index
	void buildEdges(const MSA &msa) {
		for(unsigned int n = 0; n < tree.size(); n++) {
			auto [m,t] = tree[n];
			if(m == -1) {
				root_index = n;
				continue;
			}
			if(n < msa.size()) { //This is a leave node
				nodes[n].data = msa[n][col];
				nodes[n].hidden = false;
				nodes[n].t = t;
			} else { //This is a hidden node
				nodes[n].hidden = true;
				nodes[n].t = t;
			}
			//Assign the connections between nodes
			if(nodes[m].next[0] == -1) {
				nodes[m].next[0] = n;
			} else {
				nodes[m].next[1] = n;
			}
		}
	}

public:
	std::vector<FelsensteinNode<F>> nodes;
	unsigned int root_index = 0;
	unsigned int col = 0;
	PhyloTree tree;

	//The Tree has to be rooted on a leave node!
	Felsenstein(const MSA &msa, const PhyloTree &tree, int col) : nodes(tree.size()), col(col), tree(tree) {
		buildEdges(msa);
	}

	template<typename Transition>
	void calculate_edges(Transition &trans) {
		//This does calculate recursively all the others
		calculate_edge(root_index, trans);
	}
};

#endif //EMS_FELSENSTEIN_H
