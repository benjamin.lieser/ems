//
// Created by benjamin on 10.06.20.
//

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include "common_types.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/array.hpp>


namespace py = pybind11;

bool serialize(const py::array_t<int,py::array::c_style | py::array::forcecast> &msa, const py::array_t<double,py::array::c_style | py::array::forcecast> &tree, double lambda, const std::string &filename) {


	//Copy the MSA into our MSA format
	py::buffer_info bufMSA = msa.request();

	assert(bufMSA.ndim == 2);

	int N = bufMSA.shape[0];
	int L = bufMSA.shape[1];

	auto *MSA_prt = (int*) bufMSA.ptr;

	MSA msaIntern(N, std::vector<amino>(L));

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < L; j++) {
			msaIntern[i][j] = MSA_prt[i*L + j];
		}
	}

	//Copy the tree into our Tree format

	py::buffer_info bufTree = tree.request();

	assert(bufTree.ndim == 2);
	assert(bufTree.shape[1] == 2);

	int TreeSize = bufTree.shape[0];

	PhyloTree treeIntern(TreeSize);

	auto *Tree_prt = (double*) bufTree.ptr;

	for(int i = 0; i < TreeSize ; i++) {
		treeIntern[i] = {Tree_prt[2*i], Tree_prt[2*i + 1]};
	}

	std::ofstream ofs(filename);

	boost::archive::text_oarchive oa(ofs);

	oa << msaIntern;
	oa << treeIntern;
	oa << lambda;

	return true;
}

PYBIND11_MODULE(pythonInput, m) {
m.doc() = "Serialize EM Input"; // optional module docstring

m.def("serialize", &serialize, "serialize");
}