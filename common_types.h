//
// Created by benjamin on 01.03.21.
//

#ifndef EMS_COMMON_TYPES_H
#define EMS_COMMON_TYPES_H

#include <random>

#include "A.h" //Number of amino acids. They are numbered from 0 to A-1.
using amino = uint8_t; //Type of amino acids

const amino gap_amino = 42;

const double log0 = -1e10; //This is our zero in logspace.

using MSA = std::vector<std::vector<amino>>;//First dimension: The sequences, Second dimension The data of each sequence

using PhyloTree = std::vector<std::pair<int, double>>; //Each entry is a node with parent id and time. The root has parent -1

using default_rng_engine = std::mt19937;

template<typename F>
using A_array = std::array<F,A>;

template<typename F>
using AA_array = std::array<A_array<F>,A>;

#endif //EMS_COMMON_TYPES_H
