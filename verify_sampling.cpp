//
// Created by benjamin on 17.03.21.
//

#include <iostream>
#include <random>
#include <fstream>
#include <torch/torch.h>

#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/array.hpp>

#include <boost/container_hash/hash.hpp>

#include "common_types.h"
#include "common_functions.h"
#include "sampling_models.h"
#include "derivatives_auto.h"
#include "Felsenstein.h"

template<typename RNG>
void sample_rec(RNG &rng, MSA &msa, PhyloTree &tree, unsigned int col, unsigned int index, at::Tensor r, at::Tensor f, U<double> &u, std::vector<double> &space) {
	if(msa[index][col] != gap_amino) return;
	auto [parent, time] = tree[index];
	sample_rec(rng, msa, tree, col, parent, r, f, u, space);
	amino x = msa[parent][col];
	for(amino y = 0; y < A; y++) {
		if(y != x) {
			space[y] = f[col].item<double>() * u.u_i(x, y) * time * std::exp(0.5*(r[col][y] - r[col][x]).item<double>());
		} else {
			double sum = 0;
			for(amino a = 0; a < A; a++) {
				if(a == x) continue;
				sum += f[col].item<double>() * u.u_i(x, a) * time * std::exp(0.5*(r[col][a] - r[col][x]).item<double>());
			}
			space[y] = std::exp(-sum);
		}
	}
	std::discrete_distribution<amino> dist(space.begin(), space.end());

	msa[index][col] = dist(rng);
}

template<typename RNG>
std::vector<MSA> gen_samples(RNG &rng, const MSA &msa, const PhyloTree &tree, at::Tensor r, at::Tensor f, int S) {

	//Do this to get the tree used by the algorithm, it has root on a leave node
	Felsenstein<double> felsenstein(msa, tree, 0);
	PhyloTree tree1 = felsenstein.tree;
	unsigned int root_index = felsenstein.root_index;

	unsigned int L = msa[0].size();
	unsigned int NN = tree1.size();
	unsigned int N = msa.size();

	std::vector<MSA> result(S);

	MSA root_msa(tree1.size(), std::vector<amino>(L, gap_amino));

	for(unsigned int i = 0; i < L; i++) {
		root_msa[root_index][i] = msa[root_index][i];
	}

	std::vector<double> space(A);
	U<double> u;

	for(int s = 0; s < S; s++) {
		while(true) {
			try_again:
			result[s] = root_msa;
			for(unsigned int col = 0; col < L; col++) {
				for (unsigned int node = 0; node < NN; node++) {
					sample_rec(rng, result[s], tree1, col, node, r, f, u, space);
				}
				//If the result does not matches on the leave sample again
				for(unsigned int node = 0; node < N; node++) {
					if(result[s][node][col] != msa[node][col]) goto try_again;
				}
			}
			break;
		}
	}
	return result;
};



std::vector<MSA> gen_all_samples(const MSA &msa, const PhyloTree &tree) {
	assert(A == 2);
	//Constants of the Input
	const unsigned int L = msa[0].size(); //Number of columns
	const unsigned int N = msa.size();    //Number of leaves
	const unsigned int NN = tree.size();  //Number of nodes
	const unsigned int P = L*(L-1)/2;      //Number of column pairs

	const unsigned int count = std::pow(A, (NN - N) * L);

	std::vector<MSA> result(count);


	for(unsigned int i = 0; i < count; i++) {
		result[i] = msa;
		//extend the msa for the hidden nodes
		for(unsigned int node = N; node < NN; node++) {
			result[i].emplace_back(L);
			for(unsigned int col = 0; col < L; col++) {
				unsigned int index = (node - N) * L + col;
				//Use here the binary representation of i
				result[i][node][col] = ((1<<index) & i) > 0 ? 1 : 0;
			}
		}
	}
	return result;
}

std::unordered_map<size_t , double> calculate_prob(const std::vector<MSA> &samples, const PhyloTree &tree, at::Tensor r, at::Tensor f) {
	std::unordered_map<size_t , double> map;
	for(auto &msa : samples) {
		U<double> u;
		size_t hash = boost::hash_value(msa);
		auto p = torch::exp(log_q_XZ(msa, tree, u, r, f)).item<double>();
		map[hash] = p;
	}
	return map;
}

template<typename F>
void verify(const MSA &msa, const PhyloTree &tree) {
	//Constants of the Input
	const unsigned int L = msa[0].size(); //Number of columns
	const unsigned int N = msa.size();    //Number of leaves
	const unsigned int NN = tree.size();  //Number of nodes
	const unsigned int P = L*(L-1)/2;      //Number of column pairs

	//Parameters we want to optimize

	//Theta
	auto v = torch::rand({L, A}, torch::TensorOptions().dtype(torch::kFloat64));
	auto w = torch::rand({P, A, A}, torch::TensorOptions().dtype(torch::kFloat64));

	//Phi
	auto r = torch::rand({L, A}, torch::TensorOptions().dtype(torch::kFloat64));
	auto f = torch::rand({L}, torch::TensorOptions().dtype(torch::kFloat64));
	f-= 1;

	U<F> u;
	default_rng_engine rng(43);

	auto all_samples = gen_all_samples(msa, tree);

	auto map = calculate_prob(all_samples, tree, r, f);

	variational_model<double> model(msa, tree, u, r, f);

	unsigned int S = 10000000;

	auto samples = model.template sample(rng, S);

	std::unordered_map<size_t, double> emp_map;
	for(auto &sample : samples) {
		auto hash = boost::hash_value(sample);
		emp_map[hash] += 1.0;
	}
	double sum = 0;
	for(auto prob : map) {
		sum += prob.second;
	}
	for(auto &prob : map) {
		prob.second /= sum;
		std::cout <<  prob.second*S << " " << emp_map[prob.first] << std::endl;
	}
}


int main(int argc, char *argv[]) {
	if (argc < 2) {
		std::cerr << "Please provide one argument, the test case filename" << std::endl;
	}
	std::string inputFile(argv[1]);
	std::ifstream ifs(inputFile);
	boost::archive::text_iarchive ia(ifs);

	MSA msa;
	PhyloTree tree;
	double lambda;

	ia >> msa >> tree >> lambda;

	//MSA test(10, std::vector<amino>(10,2));

	verify<double>(msa, rerootTree(tree));


}
