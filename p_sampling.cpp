//
// Created by benjamin on 05.05.21.
//
#include <vector>
#include <array>
#include <iostream>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/array.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "common_types.h"
#include "common_functions.h"
#include "Theta.h"
#include "U.h"

const int Neff = 5000;

template<typename F>
F epsilon(const std::vector<amino> &seq, const Theta<F> &theta) {
	F sum = 0;
	unsigned int L = seq.size();
	for(unsigned int i = 0; i < L; i++) {
		sum += theta.v[i][seq[i]];
	}
	for(unsigned int i = 0; i < L; i++) {
		for(unsigned int j = i+1; j < L; j++) {
			sum += theta.wij(i, j, seq[i], seq[j]);
		}
	}
	return sum;
}


template<typename F>
std::vector<amino> sample_root_independent(default_rng_engine &rng, const Theta<F> &theta) {
	auto L = theta.L();
	std::vector<amino> sample(L);
	for(unsigned int i = 0; i < L; i++) {
		std::array<F,A> dist{};
		for(amino a = 0; a < A; a++) {
			dist[a] = std::exp(theta.v[i][a]);
		}
		std::discrete_distribution<int> d(dist.begin(), dist.end());
		sample[i] = d(rng);
	}
	return sample;
}

template<typename F>
std::discrete_distribution<amino> gibbs_dist(const Theta<F> &theta, const U<F> &u, const std::vector<amino> &seq, unsigned int i) {
	std::array<F, A> prob{};
	for(amino a = 0; a < A; a++) {
		F sum = 0;
		for(unsigned int j = 0; j < theta.L(); j++) {
			if(j == i) continue;
			sum += theta.wij_checked(i, j, a, seq[j]);
		}
		prob[a] = std::exp(theta.v[i][a] + std::log(u.u_tilde(a)) + sum);
	}
	return std::discrete_distribution<amino>(prob.begin(), prob.end());
}

template<typename F>
std::vector<amino> sample_root(default_rng_engine &rng, const Theta<F> &theta, const U<F> &u) {
	auto seq = sample_root_independent(rng, theta);
	std::uniform_int_distribution<unsigned int> column_dist(0, theta.L() - 1);
	for(unsigned int step = 0; step < theta.L() * 100; step++) {
		unsigned int i = column_dist(rng);
		auto dist = gibbs_dist(theta, u, seq, i);
		seq[i] = dist(rng);
	}
	return seq;
}

template<typename F>
amino mutate_u(default_rng_engine &rng, amino a, const U<F> &u) {
	std::array<F,A> prob{};
	for(amino b = 0; b < A; b++) {
		prob[b] = u.u_i(a, b);
	}
	std::discrete_distribution<amino> dist(prob.begin(), prob.end());
	return dist(rng);
}

template<typename F>
std::vector<amino> sample_child(default_rng_engine &rng, const Theta<F> &theta, const U<F> &u, const std::vector<amino> &parent, F time) {
	auto child = parent;
	unsigned int L = theta.L();
	std::poisson_distribution<unsigned int> poi(time * L * Neff);
	std::uniform_int_distribution<unsigned int> column_dist(0, L - 1);
	unsigned int M = poi(rng);
	std::uniform_real_distribution<F> u_01(0,1);
	for(unsigned int m = 0; m < M; m++) {
		unsigned int i = column_dist(rng);
		amino old = child[i];
		child[i] = mutate_u(rng, parent[i], u);
		F random = u_01(rng);
		if(random > (1.0 / Neff) * g(0.5*(epsilon(child, theta) - epsilon(parent, theta)))) { //reject mutation
			child[i] = old;
		}
	}
	return child;
}

template<typename F>
void sample_tree_rec(default_rng_engine &rng, const Theta<F> &theta, const U<F> &u, const PhyloTree &tree, MSA &msa, unsigned int node) {
	if(!msa[node].empty()) return; //already sampled
	if(tree[node].first == -1) { //node is the root
		msa[node] = sample_root(rng, theta, u);
	} else { //not a root node
		auto [parent, time] = tree[node];
		sample_tree_rec(rng, theta, u, tree, msa, parent); //Make sure the parent is sampled
		msa[node] = sample_child(rng, theta, u, msa[parent], time);
	}
}

template<typename F>
MSA sample_tree(default_rng_engine &rng, const Theta<F> &theta, const U<F> &u, const PhyloTree &tree) {
	unsigned int NN = tree.size();

	MSA msa(NN); //contains an empty vector for every node in the tree, empty vector means not yet sampled

	for(unsigned int node = 0; node < NN; node++) {
		sample_tree_rec(rng, theta, u, tree, msa, node);
	}

	return msa;
}

unsigned int count_Leaves(const PhyloTree &tree) {
	std::vector<bool> is_leave(tree.size(), true);
	for(auto[m, t] : tree) {
		if(m == -1) continue;
		is_leave[m] = false;
	}
	return std::count(is_leave.begin(), is_leave.end(), true);
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		std::cerr << "Please provide one argument, the config file" << std::endl;
		return 1;
	}

	boost::property_tree::ptree config;
	boost::property_tree::ini_parser::read_ini(argv[1], config);

	const auto L = config.get<unsigned int>("parameter_gen.L");

	const auto treeFile = config.get<std::string>("parameter_gen.tree_file");
	const auto max_edge_length = config.get<double>("miscellaneous.max_edge_length");

	std::ifstream ifs("../test_data/" + treeFile);

	unsigned int n;
	ifs >> n;
	PhyloTree tree1(n);
	for(unsigned int i = 0; i < n; i++) {
		ifs >> tree1[i].first >> tree1[i].second;
	}

	auto tree = rerootTree(splitTree(tree1, max_edge_length));

	default_rng_engine rng(43);
	Theta<double> theta(L);
	const auto theta_generation_method = config.get<std::string>("parameter_gen.theta_generate_method");
	if(theta_generation_method == "contact") {
		const auto stddev_w = config.get<double>("parameter_gen.stddev_w");
		const auto stddev_v = config.get<double>("parameter_gen.stddev_w");
		const auto contact_prob = config.get<double>("parameter_gen.contact_prob");
		theta.random_contact(rng, stddev_v, stddev_w, contact_prob);
	} else if (theta_generation_method == "gaussian") {
		const auto stddev_w = config.get<double>("parameter_gen.stddev_w");
		const auto stddev_v = config.get<double>("parameter_gen.stddev_w");
		theta.random(rng, stddev_v, stddev_w);
	} else {
		std::cout << "No valid theta_generate_method" << std::endl;
		return 1;
	}

	U<double> u;

	auto simulation = sample_tree(rng, theta, u, tree);

	//Remove intermediate nodes
	simulation.resize(count_Leaves(tree1));

	std::cout << simulation.size() << std::endl;

	std::ofstream ofs("simulation");

	boost::archive::text_oarchive oa(ofs);

	oa << simulation << tree << theta;

}