//
// Created by benjamin on 06.03.21.
//

#ifndef EMS_DERIVATIVES_AUTO_H
#define EMS_DERIVATIVES_AUTO_H

#include <iostream>

#include <torch/torch.h>

#include "common_types.h"
#include "U.h"
#include "Theta.h"
#include "Phi.h"

at::Tensor g(const at::Tensor &x) {
	//TODO this does probably not work well with gradients when x = 0
	if (x.item<double>() == 0) {
		return torch::ones({1}, torch::TensorOptions().dtype(torch::kFloat64));
	} else {
		return 2*x / (1-torch::exp(-2*x));
	}
}

at::Tensor log_g(const at::Tensor &x) {
	/*auto value = x.item<double>();
	if(std::abs(value) < 1e-10) { // The function is approx e^x near 0
		return x;
	} else if (value > 0) {
		return torch::log(x) + std::log(2) - torch::log(1-torch::exp(-2*x));
	} else {
		return torch::log(-x) + std::log(2) - torch::log(torch::exp(-2*x) - 1);
	}*/
	//TODO simple version this is just for verification
	return torch::log(g(x));
}

at::Tensor epsilon(const std::vector<amino> &seq, const at::Tensor &w, const at::Tensor &v, const std::vector<std::pair<unsigned int, unsigned int>> &pair_ids) {
	/*auto index = torch::zeros({(long)seq.size(), A});

	auto idx = torch::from_blob((void*)seq.data(), {(long)seq.size()}, torch::TensorOptions().dtype(torch::kInt8)).to(torch::kInt64);

	auto v_sum = v.gather(1, idx.unsqueeze(1)).sum();
	*/
	auto v_sum = torch::zeros({1}, torch::TensorOptions().dtype(torch::kFloat64));
	for(unsigned int i = 0; i < seq.size(); i++) {
		v_sum += v[i][seq[i]];
	}

	auto w_sum = torch::zeros({1}, torch::TensorOptions().dtype(torch::kFloat64));

	for(unsigned int id = 0; id < pair_ids.size(); id++) {
		auto [i,j] = pair_ids[id];
		w_sum += w[id][seq[i]][seq[j]];
	}
	return v_sum + w_sum;
}

//This function calculates ln(p(x_n | x_m, theta)) with tensors
template<typename F>
at::Tensor log_p_xn_xm_tensor(const MSA &sample, const U<F> &u, const at::Tensor &w, const at::Tensor &v, const std::vector<std::pair<unsigned int, unsigned int>> &pair_ids, unsigned int n, unsigned int m, F t) {
	auto sum = torch::zeros({1}, torch::TensorOptions().dtype(torch::kFloat64));
	unsigned int L = sample[0].size();

	auto &x = sample[m];
	auto &y = sample[n];

	for(unsigned int i = 0; i < L; i++) {
		if(x[i] != y[i]) {
			std::vector<amino> x_mod = x;
			x_mod[i] = y[i];
			sum += std::log(u.u_i(x[i], y[i])) + std::log(t) + torch::log(g(0.5 * (epsilon(x_mod, w, v, pair_ids) - epsilon(x, w, v, pair_ids))));
		} else {
			for(amino a = 0; a < A; a++) {
				if(a == x[i]) continue;
				std::vector<amino> x_mod = x;
				x_mod[i] = a;
				sum -= u.u_i(x[i], a) * t * g(0.5 * (epsilon(x_mod, w, v, pair_ids) - epsilon(x, w, v, pair_ids)));
			}
		}
	}
	return sum;
}

//This function calculates the derivative of ELBO wrt v and w. The function writes the derivative into the gradients of v and w.
template<typename F>
void d_theta_auto_one_sample(const MSA &sample, const PhyloTree &tree, const U<F> &u, at::Tensor v, at::Tensor w) {
	static_assert(std::is_same<F, double>::value, "For now this only works on double");

	unsigned int L = sample[0].size();

	//pair_ids
	std::vector<std::pair<unsigned int, unsigned int>> pair_ids;
	pair_ids.reserve(L*(L-1)/2);
	for(unsigned int i = 0; i < L; i++) {
		for(unsigned int j = i + 1; j < L; j++) {
			pair_ids.emplace_back(i, j);
		}
	}

	auto p = torch::zeros({1},torch::TensorOptions().dtype(torch::kFloat64)); //Here we calculate p(X,Z | theta)

	unsigned int r = 0; //index of the root

	for(unsigned int y = 0; y < tree.size(); y++) {
		auto [x, t] = tree[y];
		if(x == -1) { //Root node
			r = y;
			continue; //We do not calculate this term because we cannot calculate Z. We derive the derivative analytically, but we save the root index
		} else {
			p += log_p_xn_xm_tensor(sample, u, w, v, pair_ids, y, x, t);
		}
	}

	p.backward();

	//Calculate the gradient of the root index and add it to the gradient

	for(unsigned int index = 0; index < pair_ids.size(); index++) {
		auto [i,j] = pair_ids[index];
		F sum = 0;
		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				sum += torch::exp(v[i][a] + v[j][b] + w[index][a][b]).item<double>();
			}
		}

		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				if(sample[r][i] == a && sample[r][j] == b) {
					w.grad()[index][a][b] += 1;
				}
				w.grad()[index][a][b] -= torch::exp(v[i][a] + v[j][b] + w[index][a][b]) / sum;
			}
		}
	}

	for(unsigned int i = 0; i < L; i++) {
		F sum = 0;
		for(amino a = 0; a < A; a++) {
			sum += std::exp(v[i][a].item<double>());
		}
		for(amino a = 0; a < A; a++) {
			if(sample[r][i] == a) {
				v.grad()[i][a] += 1;
			}
			v.grad()[i][a] -= torch::exp(v[i][a]) / sum;
		}
	}
}

template<typename F>
void d_theta_auto(const std::vector<MSA> &samples, const PhyloTree &tree, const U<F> &u, at::Tensor v, at::Tensor w) {
	v.requires_grad_(true);
	w.requires_grad_(true);

	for(auto &sample : samples) {
		d_theta_auto_one_sample(sample, tree, u, v, w);
	}
	v.mutable_grad() /= -(double)samples.size();
	w.mutable_grad() /= -(double)samples.size();
}


//This function calculates ln(q(x_n | x_m, theta, psi)) with tensors
template<typename F>
at::Tensor log_q_xn_xm_tensor(const MSA &sample, const U<F> &u, const at::Tensor &r, const at::Tensor &f, unsigned int n, unsigned int m, F t) {
	auto sum = torch::zeros({1}, torch::TensorOptions().dtype(torch::kFloat64));
	unsigned int L = sample[0].size();

	for(unsigned int i = 0; i < L; i++) {
		amino x_i = sample[m][i];
		amino y_i = sample[n][i];
		if(x_i != y_i) {
			sum += f[i] + std::log(u.u_i(x_i, y_i)) + std::log(t) + log_g(0.5*(r[i][y_i] - r[i][x_i]));
		} else {
			for(amino a = 0; a < A; a++) {
				if(a == x_i) continue;
				sum -= torch::exp(f[i]) * u.u_i(x_i, a) * g(0.5* (r[i][a] - r[i][x_i])) * t;
			}
		}
	}

	return sum;
}


//This function calculates ln(q(X,Z| phi, psi)) with tensors, so we can get the result and the gradient
template<typename F>
at::Tensor log_q_XZ(const MSA &sample, const PhyloTree &tree, const U<F> &u, const at::Tensor &r, const at::Tensor &f) {
	auto sum = torch::zeros({1}, torch::TensorOptions().dtype(torch::kFloat64));
	unsigned int L = sample[0].size();
	for(unsigned int n = 0; n < tree.size(); n++) {
		auto [m, t] = tree[n];
		auto &xn = sample[n];
		auto &xm = sample[m];
		if(m == -1) {
			for(unsigned int i = 0; i < L; i++) {
				sum += r[i][xn[i]] - torch::logsumexp(r[i], 0);
			}
		} else {
			sum += log_q_xn_xm_tensor(sample, u, r, f, n, m, t);
		}
	}
	return sum;
}

template<typename F>
void d_phi_auto(const std::vector<MSA> &samples, const PhyloTree &tree, const U<F> &u, const at::Tensor &v, const at::Tensor &w, at::Tensor &r, at::Tensor &f) {
	static_assert(std::is_same<F, double>::value, "For now this only works on double");

	unsigned int S = samples.size();
	unsigned int L = samples[0][0].size();

	//pair_ids
	std::vector<std::pair<unsigned int, unsigned int>> pair_ids;
	pair_ids.reserve(L*(L-1)/2);
	for(unsigned int i = 0; i < L; i++) {
		for(unsigned int j = i + 1; j < L; j++) {
			pair_ids.emplace_back(i, j);
		}
	}

	std::vector<std::vector<F>> ln_trans(tree.size(), std::vector<F>(S, 0.0));
	std::vector<F> means(tree.size());

	for(unsigned int n = 0; n < tree.size(); n++) {
		auto [m, t] = tree[n];
		if(m == -1) continue;
		for(unsigned int s = 0; s < samples.size(); s++) {
			ln_trans[n][s] = (log_p_xn_xm_tensor(samples[s], u, w, v, pair_ids, n, m, t) -
			                  log_q_xn_xm_tensor(samples[s], u, r, f, n, m, t)).template item<F>();
		}
		means[n] = std::accumulate(ln_trans[n].begin(), ln_trans[n].end(), 0.0) / S;
	}

	r.requires_grad_(true);
	f.requires_grad_(true);

	for(unsigned int s = 0; s < samples.size(); s++) {
		F sum = 0;
		for(unsigned int n = 0; n < tree.size(); n++) {
			auto [m, t] = tree[n];
			if(m == -1) continue;
			sum += ln_trans[n][s] - means[n];
		}

		auto q = log_q_XZ(samples[s], tree, u, r, f);
		auto factor = torch::empty({1}, torch::TensorOptions().dtype(torch::kFloat64));
		factor[0] = sum;
		q.backward(factor);
	}

	r.mutable_grad() /= -(double)S;
	f.mutable_grad() /= -(double)S;
}


#endif //EMS_DERIVATIVES_AUTO_H
