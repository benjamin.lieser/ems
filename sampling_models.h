//
// Created by benjamin on 01.03.21.
//

#ifndef EMS_SAMPLING_MODELS_H
#define EMS_SAMPLING_MODELS_H

#include <random>
#include <utility>
#include <torch/torch.h>

#include "common_types.h"
#include "U.h"
#include "Felsenstein.h"


template<typename F>
struct variational_trans {
	const U<F> &u;
	std::array<F, A> r; //20 numbers
	std::vector<F> f; //L numbers
	F log(amino y, amino x, F log_t, unsigned int i) const {
		if(y != x) {
			return f[i] + u.log_u(x, y) + log_t + log_g(0.5 * (r[y] - r[x]));
		} else {
			F sum = 0;
			for(amino a = 0; a < A; a++) {
				if(a == x) continue;
				sum += u.u_i(x, a) * g(0.5 * (r[a] - r[x]));
			}
			return -std::exp(f[i]) * std::exp(log_t) * sum;
		}
	}
	//Transition from x to y / q(y|x,t)
	F operator()(amino y, amino x, F t, unsigned int i) const {
		if(y != x) {
			return std::exp(f[i]) * u.u_i(x, y) * t * g(0.5 * (r[y] - r[x]));
		} else {
			F sum = 0;
			for(amino a = 0; a < A; a++) {
				if(a == x) continue;
				//TODO t and exp(f) can be put out of the sum
				sum += std::exp(f[i]) * u.u_i(x, a) * t * g(0.5 * (r[a] - r[x]));
			}
			return std::exp(-sum);
		}
	}
	void update_r_f(at::Tensor r_t, at::Tensor f_t, unsigned int col) {
		for(amino a = 0; a < A; a++) {
			r[a] = r_t[col][a].item<F>();
		}
		for(unsigned int i = 0; i < f.size(); i++) {
			f[i] = f_t[i].item<F>();
		}
	}
	variational_trans(const U<F> &u, unsigned int L) : u(u), f(L) {
	}
};


template<typename F>
class variational_model {
private:
	at::Tensor r;
	at::Tensor f;
	std::vector<Felsenstein<F>> felsenstein{};
	std::vector<variational_trans<F >> trans{};
	const MSA &msa;
	const U<F> &u;
	unsigned int L = 0; //Number of columns
	unsigned int N = 0; //Number of leaves
	unsigned int NN = 0; //Number of nodes

	template<class RNG>
	void sample_rec(std::vector<amino> &sample, int index, RNG &rng, unsigned int col) {
		assert(index >= 0);
		if(sample[index] != gap_amino) return; //This position is already sampled

		auto [parent, time] = felsenstein[col].tree[index]; //This cannot be -1, because the root node is a leave and therefore always sampled
		sample_rec(sample, parent, rng, col);

		amino a = sample[parent];

		std::array<F,A> log_space;

		for(amino b = 0; b < A; b++) {
			log_space[b] = felsenstein[col].nodes[index].log_precomp[b] + trans[col].log(b, a, std::log(time), col);
		}

		F sum = logSumExpNat(log_space.data(), A);

		for(amino a = 0; a < A; a++) {
			log_space[a] -= sum;
			log_space[a] = std::exp(log_space[a]);
		}

		std::discrete_distribution<amino> dist(log_space.begin(), log_space.end());

		sample[index] = dist(rng);
	}

public:
	variational_model(const MSA &msa, const PhyloTree &tree, const U<F> &u, at::Tensor r_t, at::Tensor f_t) : r(std::move(r_t)), f(std::move(f_t)), msa(msa), u(u) {
		L = msa[0].size();
		N = msa.size();
		NN = tree.size();
		for(unsigned int i = 0; i < L; i++) {
			felsenstein.emplace_back(msa, tree, i);
			trans.emplace_back(u, L);
		}
	}

	template<class RNG>
	std::vector<MSA> sample(RNG &rng, unsigned int S) {
		//Do the Felsenstein with the current parameters
		for(unsigned int col = 0; col < L; col++) {
		    trans[col].update_r_f(r, f, col);
		    felsenstein[col].calculate_edges(trans[col]);
		}

		std::vector<MSA> result(S);

		//Generate rngs
		std::vector<RNG> rngs(S);
		std::uniform_int_distribution<int> seed(0, std::numeric_limits<int>::max());
		for(unsigned int s = 0; s < S; s++) {
			rngs[s] = RNG(seed(rng));
		}

		#pragma omp parallel for default(none) shared(S, result, NN, L, msa, rngs, gap_amino)
		for(unsigned int s = 0; s < S; s++) {
			result[s] = MSA(NN, std::vector<amino>(L, 0));
			for(unsigned int col = 0; col < L; col++) {
				//Init the sampling vector
				std::vector<amino> sample(NN, gap_amino);
				for(unsigned int j = 0; j < N; j++) {
					sample[j] = msa[j][col];
				}
				for(unsigned int j = 0; j < sample.size(); j++) {
					sample_rec(sample, j, rngs[s], col);
				}
				//Transposing the result
				for(unsigned int j = 0; j < NN; j++) {
					result[s][j][col] = sample[j];
				}
			}
		}
		return result;
	}
};
#endif //EMS_SAMPLING_MODELS_H
