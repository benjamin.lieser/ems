//
// Created by benjamin on 08.03.21.
//

#ifndef EMS_THETA_H
#define EMS_THETA_H


#include <vector>
#include <string>
#include <utility>
#include <iostream>
#include <fstream>
#include <torch/torch.h>


template<typename F>
struct Theta {
	std::vector<std::array<F,A>> v;
	std::vector<std::array<F, A*A>> w;
	std::vector<std::pair<unsigned int, unsigned int>> pair_ids; //Maps a column id to (i,j)
	std::vector<std::vector<unsigned int>> _pair_to_id;

	void write_csv(const std::string &filename) {
		std::ofstream file_w;
		std::ofstream file_v;
		file_w.open(filename + "_w.csv");
		file_v.open(filename + "_v.csv");
		for(auto &col : v) {
			for(F a : col) {
				file_v << a << " ";
			}
			file_v << "\n";
		}

		for(auto &pair : w) {
			for(F a : pair) {
				file_w << a << " ";
			}
			file_w << "\n";
		}
	}

	template<class Archive>
	void serialize(Archive &ar, const unsigned int) {
		ar & v;
		ar & w;
		ar & pair_ids;
		ar & _pair_to_id;
	}

	void add(const Theta<F> &other) {
	    for(unsigned int i = 0; i < v.size(); i++) {
	        for(amino a = 0; a < A; a++) {
	            v[i][a] += other.v[i][a];
	        }
	    }
	    for(unsigned int p = 0; p < w.size(); p++) {
	        for(unsigned int pair = 0; pair < A*A; pair++) {
	            w[p][pair] += other.w[p][pair];
	        }
	    }
	}

	explicit Theta(unsigned int L) : v(L), w(L*(L-1)/2), _pair_to_id(L) {
		//Reserve enough space to avoid multiple allocations
		pair_ids.reserve(L*(L-1)/2);
		for(unsigned int i = 0; i < L; i++) {
			_pair_to_id[i].resize(L);
			for(unsigned int j = i + 1; j < L; j++) {
				pair_ids.emplace_back(i, j);
				_pair_to_id[i][j] = pair_ids.size() - 1;
			}
		}
	}

	[[nodiscard]] unsigned int L() const {
		return v.size();
	}

	F wij(unsigned int i, unsigned int j, amino a, amino b) const {
		return w[_pair_to_id[i][j]][A*a + b];
	}

	F wij_checked(unsigned int i, unsigned int j, amino a, amino b) const {
		if(i == j) {
			throw std::logic_error("w_ij for i == j is not allowed");
		}
		if(i < j) {
			return wij(i,j,a,b);
		} else {
			return wij(j,i,b,a);
		}
	}

	void random_2bit(default_rng_engine &rng) {
		std::uniform_real_distribution<F> u(0,1);
		for(unsigned int i = 0; i < v.size(); i++) {
			for(amino a = 0; a < A; a++) {
				v[i][a] = u(rng) > 0.5 ? -1 : 1;
			}
		}
		for(unsigned int p = 0; p < w.size(); p++) {
			for(unsigned int pair = 0; pair < A*A; pair++) {
				w[p][pair] = u(rng) > 0.05 ? 0 : 5;
			}
		}
	}

	void random_contact(default_rng_engine &rng, F stddev_v, F stddev_w, F contact_prob) {
		std::normal_distribution<F> norm_v(0, stddev_v);
		std::normal_distribution<F> norm_w(0, stddev_w);
		std::uniform_real_distribution<F> u01;
		for(unsigned int i = 0; i < v.size(); i++) {
			for(amino a = 0; a < A; a++) {
				v[i][a] = norm_v(rng);
			}
		}
		for(unsigned int i = 0; i < L(); i++) {
			for(unsigned int j = i + 1; j < L(); j++) {
				if(u01(rng) <= contact_prob) { //Is a contact
					for(unsigned int pair = 0; pair < A*A; pair++) {
						w[pair_to_id(i, j)][pair] = norm_w(rng);
					}
				} else {
					for(unsigned int pair = 0; pair < A*A; pair++) {
						w[pair_to_id(i, j)][pair] = 0;
					}
				}
			}
		}
	}

	void random(default_rng_engine &rng, F stddev_v, F stddev_w) {
		std::normal_distribution<F> norm_v(0, stddev_v);
		std::normal_distribution<F> norm_w(0, stddev_w);
		for(unsigned int i = 0; i < v.size(); i++) {
			for(amino a = 0; a < A; a++) {
				v[i][a] = norm_v(rng);
			}
		}
		for(unsigned int p = 0; p < w.size(); p++) {
			for(unsigned int pair = 0; pair < A*A; pair++) {
				w[p][pair] = norm_w(rng);
			}
		}
	}

	[[nodiscard]] std::pair<at::Tensor, at::Tensor> to_tensor() const {
		auto ten_v = torch::zeros({(long int)v.size(), A}, torch::TensorOptions().dtype(torch::kFloat64));
		auto ten_w = torch::zeros({(long int)w.size(), A, A}, torch::TensorOptions().dtype(torch::kFloat64));
		for(unsigned int i = 0; i < v.size(); i++) {
			for(amino a = 0; a < A; a++) {
				ten_v[i][a] = v[i][a];
			}
		}
		for(unsigned int p = 0; p < w.size(); p++) {
			for(amino a = 0; a < A; a++) {
				for(amino b = 0; b < A; b++) {
					ten_w[p][a][b] = w[p][A*a+b];
				}
			}
		}
		return {ten_v, ten_w};
	}

	void from_tensor(at::Tensor ten_v, at::Tensor ten_w) {
		for(unsigned int i = 0; i < v.size(); i++) {
			for(amino a = 0; a < A; a++) {
				v[i][a] = ten_v[i][a].template item<F>();
			}
		}
		for(unsigned int p = 0; p < w.size(); p++) {
			for(amino a = 0; a < A; a++) {
				for(amino b = 0; b < A; b++) {
					w[p][a*A+b] = ten_w[p][a][b].template item<F>();
				}
			}
		}
	}

	[[nodiscard]] unsigned int pair_to_id(unsigned int i, unsigned int j) const {
		assert(i != j);
		if(j < i) {
			std::swap(i,j);
		}
		unsigned int id = _pair_to_id[i][j];
		assert(pair_ids[id] == std::make_pair(i, j));
		return id;
	}
};

#endif //EMS_THETA_H
