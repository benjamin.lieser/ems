//
// Created by benjamin on 10.06.20.
//

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <vector>
#include <tuple>
#include <iostream>

#include "common_types.h"
#include "sampling_models.h"

namespace py = pybind11;

std::tuple<py::array_t<double>,py::array_t<double>> estimateAllPython(const py::array_t<int,py::array::c_style | py::array::forcecast> &msa, const py::array_t<double,py::array::c_style | py::array::forcecast> &tree) {
	//Copy the MSA into our MSA format
	py::buffer_info bufMSA = msa.request();

	assert(bufMSA.ndim == 2);

	int N = bufMSA.shape[0];
	int L = bufMSA.shape[1];

	auto *MSA_prt = (int*) bufMSA.ptr;

	MSA msaIntern(N, std::vector<amino>(L));

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < L; j++) {
			msaIntern[i][j] = MSA_prt[i*L + j];
		}
	}

	//Copy the tree into out Tree format

	py::buffer_info bufTree = tree.request();

	assert(bufTree.ndim == 2);
	assert(bufTree.shape[1] == 2);

	int TreeSize = bufTree.shape[0];

	PhyloTree treeIntern(TreeSize);

	auto *Tree_prt = (double*) bufTree.ptr;

	for(int i = 0; i < TreeSize ; i++) {
		treeIntern[i] = {Tree_prt[2*i], Tree_prt[2*i + 1]};
	}

	//Now we can do things

	default_rng_engine rng(42);

	//Returning the result back to Python:
	const int LPairSize = L *(L-1)/2;

	std::vector<double> python_w(LPairSize*A*A);
	std::vector<double> python_v(LPairSize*A*2);

	/*for(int i = 0; i < LPairSize; i++) {
		for(amino a = 0; a < A; a++) {
			python_v[i * A*2 + a] = (double) vi[i][a];
		}

		for(amino b = 0; b < A; b++) {
			python_v[i * A*2 + A + b] = (double) vj[i][b];
		}

		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				python_w[i * A*A + a*A + b] = (double) w[i][a][b];
			}
		}
	}*/

	return {py::array_t<double>(std::vector<ptrdiff_t>{LPairSize,A,A}, python_w.data()), py::array_t<double>(std::vector<ptrdiff_t>{LPairSize, 2, A}, python_v.data())};

}


PYBIND11_MODULE(pythonBinding, m) {
m.doc() = "EM for estimating w parameters"; // optional module docstring

m.def("estimateAll", &estimateAllPython, "Estimates all parameters", py::arg("msa"), py::arg("tree"));
}