//
// Created by benjamin on 30.03.21.
//

#ifndef EMS_COMMON_FUNCTIONS_H
#define EMS_COMMON_FUNCTIONS_H

#include "common_types.h"
#include <algorithm>
#include <vector>
#include <tuple>
#include <utility>
#include <stack>
#include <fstream>
#include <queue>

#include "Theta.h"

template<typename F>
Theta<F> nij_counts(const MSA &msa) {
	unsigned int L = msa[0].size();
	Theta<F> counts(L);
	for(unsigned int i = 0; i < L; i++) {
		for(unsigned int j = i+1; j < L; j++) {
			for(amino a = 0; a < A; a++) {
				for(amino b = 0; b < A; b++) {
					for(const auto &seq : msa) {
						if(seq[i] == a && seq[j] == b) {
							counts.w[counts.pair_to_id(i, j)][a*A+b] += 1.0;
						}
					}
				}
			}
		}
	}
	return counts;
}

template<typename F>
F delta(amino a, amino b) {
	if(a == b) return 1.0;
	else return 0.0;
}

template<typename F>
void duplicate_Edge(PhyloTree &tree, unsigned int n, F new_length, unsigned int number_extra_edges) {
	auto [m, t] = tree[n];
	unsigned int current_node = m;
	for(unsigned int i = 0; i < number_extra_edges; i++) {
		tree.template emplace_back(current_node, new_length);
		current_node = tree.size() - 1;
	}
	tree[n] = {current_node, new_length};
}

template<typename F>
PhyloTree splitTree(const PhyloTree &tree, F max_length) {
	PhyloTree new_tree = tree; //copy tree

	const auto N = new_tree.size();
	for(unsigned int n = 0; n < N; n++) {
		auto [m,t] = tree[n];
		if(m == -1) continue;
		if(t <= max_length) continue;
		auto number_of_new_edges = (unsigned int)std::ceil(t / max_length) - 1; //Number of edges we need to add
		F new_length = t / (number_of_new_edges + 1);
		duplicate_Edge(new_tree, n, new_length, number_of_new_edges);
	}
	return new_tree;
}

void outputDOTFile(const PhyloTree &tree, const std::string &filename) {
	std::ofstream file;
	file.open(filename);

	file << "digraph {\n";
	for(unsigned int n = 0; n < tree.size(); n++) {
		auto [m, time] = tree[n];
		if(m == -1) continue;
		file << n << " -> " << m << "[label=\"" << time << "\"];\n";
	}
	file << "}\n";
	file.close();
}

PhyloTree rerootTree(const PhyloTree &tree) {
	//Neighbors and time
	std::vector<std::vector<std::pair<unsigned int, double>>> graph(tree.size());
	//build undirected graph of the tree
	for(unsigned int n = 0; n < tree.size(); n++) {
		auto [m,t] = tree[n];
		if(m == -1) continue;
		graph[n].emplace_back(m, t);
		graph[m].emplace_back(n, t);
	}

	//find a leave node which will we root
	unsigned int root;
	for(unsigned int n = 0; n < tree.size(); n++) {
		if(graph[n].size() == 1) { //n is a leave node
			root = n;
			break;
		}
	}

	//do a dfs:
	//node, parent, time
	std::stack<std::tuple<int, int, double>> stack;
	stack.emplace(root, -1, 0);
	PhyloTree new_tree(tree.size());
	while(!stack.empty()) {
		auto [node, parent, time] = stack.top();
		stack.pop();
		new_tree[node] = {parent, time};
		for(auto [next, t] : graph[node]) {
			if((int)next == parent) continue; //ignore the way back, we cast from unsigned int to int
			stack.emplace(next, node, t);
		}
	}

	return new_tree;
}

template<typename F>
F logSumExp(F a, F b) {
	if(a > b) {
		return a + std::log2(std::exp2(b-a) +1);
	} else {
		return b + std::log2(std::exp2(a-b)+1);
	}
}

template<typename F>
F logSumExpNat(F a, F b) {
	if(a > b) {
		return a + std::log(std::exp(b-a) +1);
	} else {
		return b + std::log(std::exp(a-b)+1);
	}
}

template<typename F>
F logSubExp(F a, F b) {
	if(b > a) {
		std::cerr << a << " " << b << std::endl;
		std::cerr << "Cant substract this in logspace" << std::endl;
		exit(1);
	} else if(b == a) {
		return log0;
	}
	return a + std::log2(-std::exp2(b -a) +1);
}

template<typename F>
F logSumExpNat(const F *x, unsigned int n) {
	F maxValue = *std::max_element(x, x+n);
	F logInside = 0;
	for(unsigned int i = 0; i < n; i++) {
		logInside += std::exp(x[i] - maxValue);
	}
	return maxValue + std::log(logInside);
}

template<typename F>
F logSumExp(const F *x, unsigned int n) {
	F maxValue = *std::max_element(x, x+n);
	F logInside = 0;
	for(unsigned int i = 0; i < n; i++) {
		logInside += std::exp2(x[i] - maxValue);
	}
	return maxValue + std::log2(logInside);
}

template<typename F>
F g(F x) {
	static_assert(std::is_same<F,double>::value);
	return std::abs(x) < 1e-6 ? 1.0/(1.0-x) : 2.0*x/(1.0-std::exp(-2.0*x));
}

template<typename F>
F d_g(F x) {
	static_assert(std::is_same<F,double>::value);
	if(std::abs(x) < 1e-5) {
		//linear Taylor (second order is zero)
		return 1 + (2*x / 3);
	} else if (x > 0) {
		F exp = std::exp(-2.0*x);
		return 2.0 * ( 1.0 - exp*(2.0*x + 1.0) ) /  (1.0 - exp) / (1.0 - exp);
	} else {
		F exp = std::exp(2.0*x);
		return 2.0*exp*(-2.0*x + exp - 1.0) / (1.0 - exp) / (1.0 - exp);
	}
}

template<typename F>
F log_g(F x) {
	static_assert(std::is_same<F,double>::value);
	if(std::abs(x) < 1e-6) { // The function is approx e^x near 0
		return x;
	} else if (x > 0) {
		return std::log(x) + std::log(2) - std::log(1-std::exp(-2*x));
	} else {
		return std::log(-x) + std::log(2) - std::log(std::exp(-2*x) - 1);
	}
}

void print_MSA(const MSA &msa) {
	for(auto &seq : msa) {
		for(auto amino : seq) {
			std::cout << (int) amino << " ";
		}
		std::cout << std::endl;
	}
}

#endif //EMS_COMMON_FUNCTIONS_H
