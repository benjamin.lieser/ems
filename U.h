//
// Created by benjamin on 01.03.21.
//

#ifndef EMS_U_H
#define EMS_U_H

#include<array>

#include "common_types.h"

template<typename F>
struct U {
private:
	std::array<F, A * A> u;
	std::array<F, A> u_tilde_d;
public:
	F u_i(amino a, amino b) const {
		return u[a * A + b];
	}

	F log_u(amino a, amino b) const {
		return std::log(u_i(a,b));
	}

	F u_tilde(amino a) const {
		return u_tilde_d[a];
	}

	U() {
		u.fill(1.0 / A);
		u_tilde_d.fill(1.0 / A);
	}
};

#endif //EMS_U_H
