#!/bin/bash
mkdir $1/build
cd $1/build
cmake -DCMAKE_BUILD_TYPE=Release ../..
make extractMSA
cd ..
./build/extractMSA simulation > msa.aln
