//
// Created by benjamin on 13.03.21.
//

#ifndef EMS_PHI_H
#define EMS_PHI_H

//For now this is only for the variational model, we might template this later for the autoregressive model
template<typename F>
struct Phi {
	std::vector<std::array<F,A>> r;
	std::vector<F> f;

	Phi(const std::vector<std::array<F,A>> &r, const std::vector<F> &f) : r(r), f(f) {
	}

	explicit Phi(unsigned int L) : r(L), f(L) {
	}

	void add(const Phi<F> &other) {
	    for(unsigned int i = 0; i < r.size(); i++) {
	        for(amino a = 0; a < A; a++) {
	            r[i][a] += other.r[i][a];
	        }
	    }
	    for(unsigned int i = 0; i < f.size(); i++) {
	        f[i] += other.f[i];
	    }
	}

	[[nodiscard]] std::pair<at::Tensor, at::Tensor> to_tensor() const {
		auto ten_r = torch::zeros({(long int)r.size(), A}, torch::TensorOptions().dtype(torch::kFloat64));
		auto ten_f = torch::zeros({(long int)f.size()}, torch::TensorOptions().dtype(torch::kFloat64));
		for(unsigned int i = 0; i < r.size(); i++) {
			for(amino a = 0; a < A; a++) {
				ten_r[i][a] = r[i][a];
			}
		}
		for(unsigned int i = 0; i < f.size(); i++) {
			ten_f[i] = f[i];
		}
		return {ten_r, ten_f};
	}

	void from_tensor(at::Tensor ten_r, at::Tensor ten_f) {
		for(unsigned int i = 0; i < r.size(); i++) {
			for(amino a = 0; a < A; a++) {
				r[i][a] = ten_r[i][a].template item<F>();
			}
		}
		for(unsigned int i = 0; i < f.size(); i++) {
			f[i] = ten_f[i].template item<F>();
		}
	}

	bool equals_v_r(const Theta<F> &other, F tol = 1e-10) {
        for(unsigned int i = 0; i < r.size(); i++) {
            for(amino a = 0; a < A; a++) {
                if(std::abs(r[i][a] - other.v[i][a]) > tol) return false;
            }
        }
        return true;
	}
};

#endif //EMS_PHI_H
