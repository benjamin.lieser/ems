//
// Created by benjamin on 01.03.21.
//

#include <iostream>
#include <fstream>
#include <torch/torch.h>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/array.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "common_types.h"
#include "sampling_models.h"
#include "derivatives_auto.h"
#include "optimize.h"

int main(int argc, char *argv[]) {
    //omp_set_num_threads(1);

	if (argc < 3) {
		std::cerr << "Please provide tow arguments, the config filename and the simulation file" << std::endl;
		return 1;
	}

	boost::property_tree::ptree config;
	boost::property_tree::ini_parser::read_ini(argv[1], config);

	std::string inputFile(argv[2]);
	std::ifstream ifs(inputFile);
	boost::archive::text_iarchive ia(ifs);

	MSA msa;
	PhyloTree tree;

	ia >> msa >> tree;

	auto counts = nij_counts<double>(msa);

	counts.write_csv("counts");

	unsigned int L = msa[0].size();

	Theta<double> real_theta(L);

	ia >> real_theta;

	auto estimated_theta = optimize<double>(msa, splitTree(tree, config.get<double>("miscellaneous.max_edge_length")) , real_theta, config);

	real_theta.write_csv("real_theta");
	estimated_theta.write_csv("estimated_theta");
}