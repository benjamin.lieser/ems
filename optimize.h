//
// Created by benjamin on 15.03.21.
//

#ifndef EMS_OPTIMIZE_H
#define EMS_OPTIMIZE_H

#include <exception>
#include <chrono>

#include <torch/torch.h>

#include "common_types.h"
#include "common_functions.h"
#include "derivatives.h"
#include "sampling_models.h"

template<class timepoint>
double duration(timepoint t1, timepoint t2) {
	return std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count() / 1e6;
}

template<typename F>
at::Tensor sparse_group_penalty_gradient(at::Tensor w, F lambda) {
	auto grad = w.clone();
	for(unsigned int p = 0; p < w.size(0); p++) {
		F sum = 0;
		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				sum += w[p][a][b].template item<F>() * w[p][a][b].template item<F>();
			}
		}
		if(sum < 1e-12) { // The values are all to small
			continue;
		}
		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				grad[p][a][b] *= lambda / std::sqrt(sum);
			}
		}
	}
	return grad;
}

void ss_to_file(const std::vector<std::stringstream> &data, const std::string &file_name) {
	std::ofstream file;
	file.open(file_name);
	for(auto &ss : data) {
		file << ss.str() << std::endl;
	}
}

void write_v_to_ss(std::vector<std::stringstream> &data, at::Tensor tensor) {
	unsigned int L = tensor.size(0);
	for(unsigned int i = 0; i < L; i++) {
		for(amino a = 0; a < A; a++) {
			data[i * A + a] << tensor[i][a].item<double>() << " ";
		}
	}
}

void write_w_to_ss(std::vector<std::stringstream> &data, at::Tensor tensor) {
	unsigned int P = tensor.size(0);
	for(unsigned int p = 0; p < P; p++) {
		for(amino a = 0; a < A; a++) {
			for(amino b = 0; b < A; b++) {
				data[p * A * A + a * A + b] << tensor[p][a][b].item<double>() << " ";
			}
		}
	}
}

template<typename F>
at::Tensor starting_v(const MSA &msa) {
	const unsigned int L = msa[0].size();
	auto v = torch::zeros({L, A}, torch::TensorOptions().dtype(torch::kFloat64));
	for(unsigned int i = 0; i < L; i++) {
		std::array<unsigned int, A> count{};
		//TODO one pseudocount of each amino acid so we do not divide by zero
		count.fill(1);
		for(auto &seq : msa) {
			count[seq[i]]++;
		}
		for(amino a = 0; a < A; a++) {
			v[i][a] = std::log(count[a]);
		}
	}
	return v;
}

template<typename F>
Theta<F> optimize(const MSA &msa, const PhyloTree &orig_tree, const Theta<F> &truth, const boost::property_tree::ptree &config) {
	//Constants of the Input
	const unsigned int L = msa[0].size(); //Number of columns
	//const unsigned int N = msa.size();    //Number of leaves
	//const unsigned int NN = tree.size();  //Number of nodes
	const unsigned int P = L*(L-1)/2;      //Number of column pairs

	auto tree = rerootTree(orig_tree);

	//Parameters we want to optimize

	//Theta
	at::Tensor v;
	at::Tensor w;

	if(config.get<bool>("optimization.start_with_truth")) {
		auto pair = truth.to_tensor();
		v = pair.first;
		w = pair.second;
	} else {
		v = starting_v<F>(msa).clone();
		w = torch::zeros({P, A, A}, torch::TensorOptions().dtype(torch::kFloat64));
	}

	//Phi
	auto f = torch::zeros({L}, torch::TensorOptions().dtype(torch::kFloat64));
	auto r = v.clone();

	std::vector<at::Tensor> para = {v,r,f,w};

	torch::optim::Adam adam(para, torch::optim::AdamOptions(1e-2));

	U<F> u;
	default_rng_engine rng(42);

	variational_model<F> q(msa, tree, u, r, f);

	std::vector<std::stringstream> w_ss(P * A * A);

	for(int step = 0; step < config.get<int>("optimization.steps"); step++) { //Optimize Loop
		auto t0 = std::chrono::high_resolution_clock::now();
	    adam.zero_grad();

		write_w_to_ss(w_ss, w);

		auto t1 = std::chrono::high_resolution_clock::now();
		auto samples = q.sample(rng, config.get<int>("optimization.number_of_samples"));
		auto t2 = std::chrono::high_resolution_clock::now();
		d_theta(samples, tree, u, v, w);
		auto t3 = std::chrono::high_resolution_clock::now();
		d_phi(samples, tree, u, v, w, r, f);
		auto t4 = std::chrono::high_resolution_clock::now();

		//regularisation
		F lambda_w = config.get<F>("regularization.lambda_w");
		F lambda_r = config.get<F>("regularization.lambda_r");
		F lambda_v = config.get<F>("regularization.lambda_v");

		const auto w_method = config.get<std::string>("regularization.w_method");

		if(w_method == "group_lasso") {
			w.mutable_grad() += sparse_group_penalty_gradient(w, lambda_w);
		} else if(w_method == "gaussian") {
			w.mutable_grad() += 0.5 * lambda_w * w;
		}

		r.mutable_grad() += 0.5 * lambda_r * r;
		v.mutable_grad() += 0.5 * lambda_v * v;



		std::cout << "step: " << step + 1 << std::endl;

		std::cout << "v " << v.grad().norm().template item<double>() << std::endl;
		std::cout << "w " << w.grad().norm().template item<double>() << std::endl;

		std::cout << "r " << r.grad().norm().template item<double>() << std::endl;
		std::cout << "f " << f.grad().norm().template item<double>() << std::endl;

		//std::cout << v - r << std::endl;
		//std::cout << r.grad() << std::endl;

		std::cout << std::endl;

		adam.step();

		if(!torch::all(f < 0).item<bool>()) {
			std::cout << f << std::endl;
		}
        auto t5 = std::chrono::high_resolution_clock::now();
		std::cout << "All: " << duration(t0, t5) << "\n";
		std::cout << "Sampling: " << duration(t1, t2) << "\n";
		std::cout << "d_theta: " << duration(t2, t3) << "\n";
		std::cout << "d_phi: " << duration(t3, t4) << "\n";
	}
	ss_to_file(w_ss, "w_log");
	Theta<F> result(L);
	result.from_tensor(v, w);
	return result;
}


#endif //EMS_OPTIMIZE_H
